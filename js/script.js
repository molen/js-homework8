let ourInput = document.createElement("input");
document.body.appendChild(ourInput);
ourInput.placeholder = "Price";
ourInput.classList.add('default-input');

let ourContainer = document.createElement('div');

let ourSpan = document.createElement('span');

let closeButton = document.createElement('a');
closeButton.href = '#';
closeButton.classList.add('close');

let errorMsg = document.createElement("p");
errorMsg.innerText = 'Please enter correct price';
errorMsg.style = 'color: red;';

ourInput.addEventListener('focus', () => {
    ourInput.style = 'border-color: green;';
    errorMsg.style = 'display: none';
});

ourInput.addEventListener('blur', () => {
    ourInput.style = 'border-color: black;';
    ourInput.before(ourContainer);
    ourContainer.appendChild(ourSpan);
    ourSpan.after(closeButton);
    ourSpan.innerText = `Текущая цена:$${ourInput.value}`;
    ourInput.style = 'color: green';
    ourContainer.style = "display: block;";
    errorMsg.style = 'display: none';
    if (ourInput.value < 0) {
        ourInput.style = 'border-color: red;';
        ourInput.after(errorMsg);
        errorMsg.style = 'color: red;';
        ourContainer.style = 'display: none;';

    }
});

closeButton.addEventListener('click', () => {
    ourContainer.style = "display: none;";
    ourInput.value = '';
});


// разрешаем вводить только цифры, '-', 'tab', 'backspace'.

let [rangeFrom, rangeTo] = [48, 57];
let  exceptions = [8,9,189];

ourInput.onkeydown = (event) => {
    let key = event.keyCode;
    let errorMessage = `${event.key} is not number, please enter number`;

    if (!~exceptions.indexOf(key) && (key < rangeFrom || key > rangeTo)) {
        console.log(errorMessage);
        return false;
    }
};
